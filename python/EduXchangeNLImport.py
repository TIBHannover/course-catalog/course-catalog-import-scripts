from EduXchangeNLMapping import EduXchangeNLMapping
from eduXchangeHelper import EduXchangeImportBase


class EduXchangeNLImport(EduXchangeImportBase):
    def __init__(self):
        super().__init__("eduXchange.nl", "https://eduxchange.nl",
                         [
                             # "/en/for-students-uu/explore.json",
                             # "/en/for-students-wur/explore.json",
                             # "/en/for-students-tue/explore.json",
                             # "/en/for-students-eur/explore.json",
                             # "/en/for-students-tud/explore.json",
                             # "/en/for-students-ul/explore.json",
                             "/en/guest/explore.json",
                         ],
                         False,
                         EduXchangeNLMapping())

    def get_records(self, json_resp):
        return json_resp["pageProps"]["items"]

    def get_record_id(self, record):
        return record["id"]

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        return meta


if __name__ == "__main__":
    EduXchangeNLImport().process()
