from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class EnhanceMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("subject", {
            "Computer Science": Subject.N71_COMPUTER_SCIENCE,
            "Energy Engineering": Subject.N211_ENERGY_PROCESS_ENGINEERING,
            "Mechanical Engineering BSc": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Mechanical Engineering MSc": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Urban Planning": Subject.N67_SPATIAL_PLANNING,

            "Architecture and Urban Design": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "BSc Fundamentals of Architecture": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "BSc Mechanical Engineering": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Chemical Engineering M.Sc.": Subject.N033_CHEMICAL_ENGINEERING_CHEMICAL_PROCESS_ENGINEERING,
            "Civil Engineering": Subject.N68_CIVIL_ENGINEERING,
            "Computer Science and Information Systems": Subject.N71_COMPUTER_SCIENCE,
            "Computer Systems and Networks": Subject.N71_COMPUTER_SCIENCE,
            "Electric Power Engineering": Subject.N316_ELECTRICAL_POWER_ENGINEERING,
            "Electrical Power Engineering": Subject.N316_ELECTRICAL_POWER_ENGINEERING,
            "MATERIALS ENGINEERING": Subject.N72_MATERIALS_SCIENCE_AND_MATERIALS_ENGINEERING,
            "MSc Advanced Architecture. Landscape, Urbanism and Design": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "MSc Architecture": Subject.N66_ARCHITECTURE_INTERIOR_ARCHITECTURE,
            "MSc Civil Engineering": Subject.N68_CIVIL_ENGINEERING,
            "MSc Mechatronics Engineering": Subject.N380_MECHATRONICS,
            "Mechatronics Engineering": Subject.N380_MECHATRONICS,
        }, None)

        self.add_mapping("org_id", {
            "Chalmers University of Technology": "https://ror.org/040wg7k59",
            "Norwegian University of Science and Technology": "https://ror.org/05xg72x27",
            "Politecnico di Milano": "https://ror.org/01nffqt88",
            "RWTH Aachen": "https://ror.org/04xfq0f34",
            "Technische Universität Berlin": "https://ror.org/03v4gjf40",
            "Universitat Politècnica de València": "https://ror.org/01460j859",
            "Warsaw University of Technology": "https://ror.org/00y0xnp53"
        }, None)

        self.add_mapping("language", {
            "English": "en",
            "English friendly": "en",
            "German": "de",
            "Italian": "it",
            "Norwegian": "no",
            "Spanish": "es"
        }, None)

        self.add_mapping("educational_level", {
            "BSc": "https://w3id.org/kim/educationalLevel/level_6",
            "MSc": "https://w3id.org/kim/educationalLevel/level_7",
        })
