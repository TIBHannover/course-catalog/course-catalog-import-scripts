import re
from search_index_import_commons.Helpers import get, get_list, filter_none_values
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
from search_index_import_commons.mapping import iso639_2_to_iso639_1
from Helpers import map_credits


def get_from_offering_or_course(record, field_name, result_list=False):
    """Return the value of the specified field from the course-offering, if exists; otherwise return the value from the course"""
    src_record = record if get(record, field_name) else get(record, "course")
    if not src_record:
        return None
    return get_list(src_record, field_name) if result_list else get(src_record, field_name)


def get_person(person_record):
    if not person_record or isinstance(person_record, str):
        return None
    if "displayName" in person_record:
        name = person_record["displayName"]
    else:
        names = []
        if "givenName" in person_record:
            names.append(person_record["givenName"])
        if "surname" in person_record:
            names.append(person_record["surname"])
        name = " ".join(names) if len(names) > 0 else None
    if not name:
        return None
    orcid = get(get(person_record, "primaryCode"), "code") if get(get(person_record, "primaryCode"), "codeType") == "orcId" else None
    return filter_none_values({
        "name": name,
        "type": "Person",
        "id": orcid
    })


class DefaultMapping(SearchIndexImportMapping):
    """Definition of default mappings. Overwrite the groups you want to change in your specific implementation"""
    def __init__(self):
        self.add_mapping("org_id", {
        }, None)
        self.add_mapping("language", iso639_2_to_iso639_1.mapping, None)
        self.add_mapping("educational_level", {
            "bachelor": "https://w3id.org/kim/educationalLevel/level_6",
            "doctoral": "https://w3id.org/kim/educationalLevel/level_8",
            "master": "https://w3id.org/kim/educationalLevel/level_7"
        })


class OpenEducationApiConverter:
    def __init__(self, preferred_language='en', mapping=None, fields_of_study_prefix=""):
        self.preferred_language = preferred_language
        self.fields_of_study_prefix = fields_of_study_prefix
        self.mapping = DefaultMapping()
        if mapping is not None:
            for group in mapping.mappings.keys():
                self.mapping.mappings[group] = mapping.mappings[group]

    def get_organizations(self, organization_records, max_level=2):
        if not organization_records or max_level <= 0:
            return None
        organizations = []
        for organization_record in organization_records:
            organization_name = self.get_value_from_language_typed_string(get(organization_record, "name"))
            organization_id = get(get(organization_record, "primaryCode"), "code")
            organization_id = organization_id if organization_id and re.match(r"https://ror.org/.+", organization_id) else self.mapping.get("org_id", organization_name)
            if organization_name:
                organizations.append(filter_none_values({
                    "name": organization_name,
                    "type": "Organization",
                    "id": organization_id,
                    "additionalType": get(organization_record, "organizationType").strip().lower() if get(organization_record, "organizationType") else None,
                    "subOrganization": self.get_organizations(get_list(organization_record, "children"), max_level=max_level-1)
                }))
        return list(filter(lambda r: r["name"] is not None, organizations))

    def get_value_from_language_typed_string(self, language_typed_string):
        if not language_typed_string:
            return None
        if isinstance(language_typed_string, list):
            valid_language_typed_string = list(filter(lambda x: "value" in x, language_typed_string))
            for entry in valid_language_typed_string:
                if "language" in entry and entry["language"].startswith(self.preferred_language):
                    return entry["value"]
            if len(valid_language_typed_string) > 0:
                return valid_language_typed_string[0]["value"]
        elif isinstance(language_typed_string, dict):
            for entry in language_typed_string.keys():
                if entry.startswith(self.preferred_language):
                    return language_typed_string[entry]
            return language_typed_string[next(iter(language_typed_string))]
        return None

    def get_language_typed_string(self, language_typed_string):
        if not language_typed_string:
            return None
        if isinstance(language_typed_string, list):
            valid_language_typed_string = list(filter(lambda x: "value" in x and "language" in x, language_typed_string))
            return list(map(lambda x: {"@language": x["language"][:2], "@value": x["value"]}, valid_language_typed_string))
        elif isinstance(language_typed_string, dict):
            return list(map(lambda x: {"@language": x[0][:2], "@value": x[1]}, language_typed_string.items()))
        return None

    def default_transformation(self, record):
        course_record = get(record, "course")
        study_load_record = get(course_record, "studyLoad")
        educational_level = self.mapping.get("educational_level", get(course_record, "level").strip().lower()) if get(course_record, "level") else None
        organization_record = get_from_offering_or_course(record, "organization")
        fields_of_study = get(course_record, "fieldsOfStudy")
        fields_of_study = (self.fields_of_study_prefix + fields_of_study) if fields_of_study else None
        subject = self.mapping.get("subject", fields_of_study) if fields_of_study and "subject" in self.mapping.mappings else fields_of_study
        instructors = get_list(course_record, "coordinators")
        course_instance = filter_none_values({
            "courseMode": list(filter(None, get_from_offering_or_course(record, "modeOfDelivery", result_list=True))),
            "courseWorkload": get(get(record, "ext"), "courseWorkload"),
            "startDate": get(record, "startDate"),
            "endDate": get(record, "endDate"),
            "instructor": list(filter(None, map(lambda x: get_person(x), instructors))),
        })
        learning_outcomes = get_list(course_record, "learningOutcomes")
        meta = {
            "@context": "https://schema.org",
            "type": ["Course"],
            "id": get_from_offering_or_course(record, "link"),
            "name": self.get_language_typed_string(get_from_offering_or_course(record, "name")),
            "description": self.get_language_typed_string(get_from_offering_or_course(record, "description")),
            "courseCode": get_from_offering_or_course(course_record, "abbreviation"),
            "about": [{"id": subject}] if subject else None,
            "inLanguage": list(filter(None, [self.mapping.get("language", get_from_offering_or_course(record, "teachingLanguage"))])),
            "numberOfCredits": map_credits(get(study_load_record, "value")) if get(study_load_record, "studyLoadUnit") == "ects" and "value" in study_load_record else None,
            "educationalLevel": [educational_level] if educational_level else None,
            "sourceOrganization": self.get_organizations([organization_record]) if organization_record else None,
            "hasCourseInstance": course_instance if course_instance else None,
            "teaches": list(filter(None, map(lambda x: self.get_value_from_language_typed_string(x), learning_outcomes))) if learning_outcomes else None,
            "coursePrerequisites": []
        }
        if get(course_record, "admissionRequirements"):
            v = self.get_value_from_language_typed_string(get(course_record, "admissionRequirements"))
            if v is not None:
                meta["coursePrerequisites"].append(v)
        if get(course_record, "qualificationRequirements"):
            v = self.get_value_from_language_typed_string(get(course_record, "qualificationRequirements"))
            if v is not None:
                meta["coursePrerequisites"].append(v)
        # just for testing
        meta["resources"] = list(filter(None, get_list(course_record, "resources")))
        return meta
