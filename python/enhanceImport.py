import re
import requests
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter
from enhanceMapping import EnhanceMapping
from Helpers import map_credits, OrganizationLogoHelper


class EnhanceImport(SearchIndexImporter):
    def __init__(self):
        super().__init__(mapping=EnhanceMapping())
        self.base_url = "https://educationpathways.enhanceuniversity.eu"
        self.available_filters = None
        self.resources_loaded = False
        self.course_ids = None
        self.items_per_Request = 1
        self.headers = {"User-Agent": self.user_agent}
        self.organization_logo_helper = OrganizationLogoHelper(self.config)

    def get_name(self):
        return "ENHANCE"

    def contains_record(self, record_id):
        return record_id.startswith(self.base_url)

    def load_single_record(self, record_id):
        url_match = re.match("^" + self.base_url + "/enhance/app/courseDetails?courseId=([a-z0-9]+)", record_id)
        if url_match:
            course_details_resp = self.load_course_details(url_match.group(1))
            if course_details_resp.status_code != 200:
                return {"response_status_code": 404}
            return course_details_resp.json()
        return None

    def load_course_details(self, course_id):
        return requests.get(self.base_url + "/enhance/rest/course/details?course_id=" + course_id, headers=self.headers)

    def load_next(self):
        if self.resources_loaded:
            return None
        if self.available_filters is None:
            filters_resp = requests.get(self.base_url + "/enhance/rest/course/filters", headers=self.headers)
            self.available_filters = {}
            for course_filter in filters_resp.json():
                self.available_filters[course_filter["filter_name"]] = list(map(lambda x: x["filter_key"], course_filter["enhanceFilterValueVOs"]))
        if self.course_ids is None:
            courses = requests.post(self.base_url + "/enhance/rest/course/", json=self.available_filters, headers=self.headers).json()
            self.course_ids = list(map(lambda x: str(x["enhance_course_id"]), courses))
        records = []
        for _ in range(self.items_per_Request):
            course_id = self.course_ids.pop()
            course_details = self.load_course_details(course_id).json()
            records.append(course_details)
            if len(self.course_ids) == 0:
                self.resources_loaded = True
                break
        return records

    def transform(self, data: list) -> list:
        return list(map(lambda record: self.enrich(self.to_search_index_metadata(record)), data))

    def enrich(self, record):
        self.organization_logo_helper.set_default_logo(record)
        return record

    def to_search_index_metadata(self, record):
        identifier = self.base_url + "/enhance/app/courseDetails?courseId=" + str(record["enhance_course_id"])
        subjects = list(set(filter(None, [self.mapping.get("subject", record["pilot_study"]), self.mapping.get("subject", record["study_course_name"])])))
        language_code = self.mapping.get("language", record["language"])
        meta = {
            "@context": "https://schema.org",
            "type": ["Course"],
            "id": identifier,
            "url": record["link_course_page"],
            "name": [{"@value": record["course_name"]}],
            "description": [{"@value": record["course_description"]}] if record["course_description"] else None,
            "courseCode": str(record["enhance_course_id"]),
            "about": list(map(lambda x: {"id": x}, subjects)),
            "educationalLevel": [self.mapping.get("educational_level", record["degree"])],
            "inLanguage": [language_code] if language_code else None,
            "keywords": record["keywords"].split("; ") if record["keywords"] else None,
            "numberOfCredits": map_credits(record["ects"]),
            "coursePrerequisites": [record["prerequisites"]] if record["prerequisites"] else None,
            "sourceOrganization": [{"name": record["university_name"], "type": "Organization", "id": self.mapping.get("org_id", record["university_name"])}],
            "mainEntityOfPage": [
                {
                    "id": identifier,
                    "provider": {
                        "id": self.base_url,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        return meta


if __name__ == "__main__":
    EnhanceImport().process()
