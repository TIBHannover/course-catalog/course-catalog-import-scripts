import re
from search_index_import_commons.Helpers import get, get_list


def map_credits(record_credits):
    if record_credits is None:
        return None
    if isinstance(record_credits, str):
        record_credits = record_credits.strip()
        if re.match(r"^\d+,5$", record_credits.strip()):
            record_credits = record_credits.replace(",", ".")
    try:
        return float(record_credits)
    except ValueError:
        return None


class OrganizationLogoHelper:
    def __init__(self, config=None):
        self.organization_logo_mapping = {
            "https://ror.org/03cx6bg69": "https://eulist.university/wp-content/themes/eulist/images/alliance/ntua.jpg",
            "https://ror.org/0304hq317": "https://eulist.university/wp-content/themes/eulist/images/alliance/luh.jpg",
            "https://ror.org/01j9p1r26": "https://eulist.university/wp-content/themes/eulist/images/alliance/aquila.jpg",
            "https://ror.org/0208vgz68": "https://eulist.university/wp-content/themes/eulist/images/alliance/lut.jpg",
            "https://ror.org/03613d656": "https://eulist.university/wp-content/themes/eulist/images/alliance/brno.jpg",
            "https://ror.org/03t54am93": "https://eulist.university/wp-content/themes/eulist/images/alliance/ju.jpg",
            "https://ror.org/0561ghm58": "https://eulist.university/wp-content/themes/eulist/images/alliance/stu.jpg",
            "https://ror.org/01v5cv687": "https://eulist.university/wp-content/themes/eulist/images/alliance/urjc.jpg",
            "https://ror.org/04d836q62": "https://eulist.university/wp-content/themes/eulist/images/alliance/tuw.jpg",
            "https://ror.org/025vp2923": "https://eulist.university/wp-content/themes/eulist/images/alliance/imt.jpg",
        }
        config_logo_mapping = get(get(config, "CourseCatalogue"), "OrganizationLogoMapping")
        if config_logo_mapping:
            self.organization_logo_mapping = {**self.organization_logo_mapping, **config_logo_mapping}

    def set_default_logo(self, metadata):
        if not metadata:
            return
        if "image" not in metadata or not metadata["image"]:
            organizations = get_list(metadata, "sourceOrganization")
            if organizations:
                ror_id = get(organizations[0], "id")
                if ror_id:
                    logo_url = self.organization_logo_mapping.get(ror_id)
                    if logo_url:
                        metadata["image"] = logo_url
