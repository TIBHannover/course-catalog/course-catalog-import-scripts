import html2text
from EnlightMapping import EnlightMapping
from OpenEducationApiHelper import OpenEducationApiImportBase


# enlight also has an API endpoint that differs from OOAPI (additional endpoint):
# query = {"pageSize": self.items_per_request, "pageNumber": self.page}
# requests.post(self.base_url + "/api/search", headers=headers, json=query)
# => that endpoint delivers course offerings with expanded values for course and organization
# But we do not use it at the moment and better use the standard endpoints


class EnlightImport(OpenEducationApiImportBase):

    def __init__(self):
        super().__init__("ENLIGHT", "https://courses.enlight-eu.org", api_base_path="/api", mapping=EnlightMapping())
        self.html2text = html2text.HTML2Text(bodywidth=0)
        self.html2text.ignore_links = True
        self.html2text.ignore_emphasis = True

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        if meta["description"] is None and "description_HTML" in record:
            meta["description"] = [{"@value": self.html2text.handle(record["description_HTML"][0]["value"]).strip()}]
        return meta


if __name__ == "__main__":
    EnlightImport().process()
