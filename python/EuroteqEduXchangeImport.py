from EuroteqEduXchangeMapping import EuroteqEduXchangeMapping
from eduXchangeHelper import EduXchangeImportBase


# NOTE: this source does not seem suitable - unstable (single resource details not reachable), changing ids


class EuroteqEduXchangeImport(EduXchangeImportBase):
    def __init__(self):
        super().__init__("EuroTeQ.eduXchange.eu", "https://eduxchange.eu",
                         [
                             # "/for-students-tue/search.json",
                             # "/for-students-taltech/search.json",
                             # "/for-students-dtu/search.json",
                             # "/for-students-lx/search.json",
                             # "/for-students-ctu/search.json",
                             # "/for-students-tum/search.json",
                             # "/for-students-technion/search.json",
                             "/en/guest/explore.json",
                         ],
                         False,
                         EuroteqEduXchangeMapping())

    def get_records(self, json_resp):
        return json_resp["pageProps"]["items"]

    def get_record_id(self, record):
        return record["id"]

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        return meta


if __name__ == "__main__":
    EuroteqEduXchangeImport().process()
