from search_index_import_commons.mapping import iso639_2_to_iso639_1
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class EduXchangeNLMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("subject", {
            "10": None,  # Interdisciplinary
            "11": Subject.N30_BUSINESS_AND_ECONOMICS,  # Economics
            "12": Subject.N26_SOCIAL_SCIENCES_SOCIOLOGY,  # Behaviour and society
            "13": Subject.N48_HEALTH_SCIENCES_GENERAL,  # Health care
            "14": Subject.N7_AGRICULTURAL_FOREST_AND_NUTRITIONAL_SCIENCES_VETERINARY_MEDICINE,  # Agriculture and natural environment
            # "15": None,  # Nature
            "16": Subject.N33_EDUCATIONAL_SCIENCES,  # Educations
            "17": Subject.N28_LAW,  # Law
            "18": Subject.N1_HUMANITIES,  # Language and culture
            # "19": None,  # Technology
        }, None)

        self.add_mapping("org_id", {
            "Delft University of Technology": "https://ror.org/02e2c7k09",
            "Eindhoven University of Technology": "https://ror.org/02c2kyt77",
            "Erasmus University Rotterdam": "https://ror.org/057w15z03",
            "Leiden University": "https://ror.org/027bh9e22",
            "Utrecht University": "https://ror.org/04pp8hn57",
            "Wageningen University & Research": "https://ror.org/04qw24q55"
        }, None)

        # self.add_mapping("image_by_org", {
        #     "Delft University of Technology": "https://eduxchange.nl/static/img/logos/delft-logo-small.svg",
        #     "Eindhoven University of Technology": "https://eduxchange.nl/static/img/logos/eindhoven-logo-small.svg",
        #     "Erasmus University Rotterdam": "https://eduxchange.nl/static/img/logos/erasmus-logo-small.svg",
        #     "Utrecht University": "https://eduxchange.nl/static/img/logos/utrecht-logo-small.svg",
        #     "Wageningen University & Research": "https://eduxchange.nl/static/img/logos/wageningen-logo-small.svg"
        # }, None)
        #
        self.add_mapping("language", iso639_2_to_iso639_1.mapping, None)

        self.add_mapping("educational_level", {
            "bachelor": "https://w3id.org/kim/educationalLevel/level_6",
            "master": "https://w3id.org/kim/educationalLevel/level_7",
        })
