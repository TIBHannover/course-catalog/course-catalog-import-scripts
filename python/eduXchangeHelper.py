from bs4 import BeautifulSoup
import abc
import html
import json
import logging
import requests
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter
from Helpers import get, map_credits, OrganizationLogoHelper


# NOTE: requires installation of non-standard lib 'python3-bs4'


class EduXchangeImportBase(SearchIndexImporter):
    def __init__(self, name, base_url, search_paths, load_details=False, mapping=None):
        super().__init__()
        self.name = name
        self.base_url = base_url
        self.search_paths = search_paths
        self.load_details = load_details
        self.mapping = mapping
        self.api_url = None
        self.processed_resource_ids = []
        self.organization_logo_helper = OrganizationLogoHelper(self.config)

    def get_name(self):
        return self.name

    def contains_record(self, record_id):
        # TODO implement
        return False

    def load_single_record(self, record_id):
        # TODO implement
        return None

    def load_next(self):
        headers = {"User-Agent": self.user_agent}
        if self.api_url is None:
            resp = requests.get(self.base_url, headers=headers)
            html_record = BeautifulSoup(resp.text, "html.parser")
            next_data_tag = html_record.find(attrs={"id": "__NEXT_DATA__", "type": "application/json"})
            build_id = json.loads(html.unescape(next_data_tag.string))["buildId"]
            logging.debug("BuildId is " + build_id)
            self.api_url = self.base_url + "/_next/data/" + build_id
            logging.debug("Using api URL " + self.api_url)

        if self.search_paths:
            search_path = self.search_paths.pop()
            search_resp = requests.get(self.api_url + search_path, headers=headers)
            records = self.get_records(search_resp.json())
            logging.debug("Found " + str(len(records)) + " resources for path " + search_path)
            # Don't process resources that were already imported for a previous path
            records = list(filter(lambda r: self.get_record_id(r) not in self.processed_resource_ids, records))
            logging.debug("Processing " + str(len(records)) + " resources for path " + search_path)
            # load details -> not used atm
            if self.load_details:
                for record in records:
                    detail_resp = requests.get(self.api_url + record["href"] + ".json", headers=headers)
                    if detail_resp.status_code != 200:
                        self.stats.add_failure()
                        logging.info("Could not fetch details for record %s: %s", record["href"], detail_resp)
                        continue
                    record["details"] = detail_resp.json()["pageProps"]["page"]["data"]
                records = list(filter(lambda x: x["details"] if "details" in x else False, records))
            self.processed_resource_ids.extend(list(map(lambda r: self.get_record_id(r), records)))
            return records
        return None

    @abc.abstractmethod
    def get_records(self, json_resp):
        """instance specific extraction of a records"""

    @abc.abstractmethod
    def get_record_id(self, record):
        """instance specific record id"""

    def transform(self, data: list) -> list:
        return list(map(lambda record: self.enrich(self.to_search_index_metadata(record)), data))

    def enrich(self, record):
        self.organization_logo_helper.set_default_logo(record)
        return record

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""

    def map_default(self, record):
        metadata_id = self.base_url + record["href"]
        meta = {
            "@context": "https://schema.org",
            "type": ["Course"],
            "id": metadata_id,
            "name": [{"@value": record["name"]}],
            "description": [{"@value": record["description"]}] if record["description"] else None,
            "courseCode": record["abbreviation"],
            "numberOfCredits": map_credits(record["ects"]),
            "mainEntityOfPage": [
                {
                    "id": metadata_id,
                    "provider": {
                        "id": self.base_url,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        subjects = set(filter(None, map(lambda s: self.mapping.get("subject", s), record["themes"]))) if record["themes"] else []
        meta["about"] = list(map(lambda s: {"id": s}, subjects))
        organization = {"type": "Organization", "name": record["configOrg"]["name"]}
        org_id = self.mapping.get("org_id", organization["name"])
        if org_id:
            organization["id"] = org_id
        meta["sourceOrganization"] = [organization]
        meta["inLanguage"] = list(filter(None, map(lambda l: self.mapping.get("language", l), record["teachingLanguages"])))
        meta["educationalLevel"] = list(filter(None, map(lambda l: self.mapping.get("educational_level", l), record["levels"])))
        meta["resources"] = get(record, "resources")
        meta["coursePrerequisites"] = []
        if get(record, "admissionRequirements"):
            meta["coursePrerequisites"].append(record["admissionRequirements"])
        if get(record, "qualificationRequirements"):
            meta["coursePrerequisites"].append(record["qualificationRequirements"])
        return meta
