import unittest
from unittest import mock
from LUHStudIpImport import LUHStudIpImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def get_test_config():
    return {
        'SEARCH_INDEX_BACKEND': {'url': 'http://localhost:8080/oersi/api/metadata', 'user': 'changeme', 'password': 'changeme'},
        'http': {'useragent': "CourseCatalogDevMetadataImportBot/0.1 (https://oersi.org/resources/services/contact) CourseCatalogSearchIndex-Import/0.1"},
        'LUH Stud.IP': {'user': 'user', 'password': 'password'}
    }


def mocked_requests_get(*args, **kwargs):
    if args[0].startswith('https://studip.uni-hannover.de/jsonapi.php/v1/study-areas/') and args[0].endswith('courses'):
        if kwargs["params"]["page[offset]"] == 0:
            with open('tests/resources/LUHStudIp_001_courses.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        else:
            return MockResponse(content="""{"meta":{"page":{"offset":0,"limit":30,"total":0}},"data":[]}""", status_code=200)
    elif args[0].startswith('https://studip.uni-hannover.de/jsonapi.php/v1/study-areas/') and args[0].endswith('children'):
        return MockResponse(content="""{"meta":{"page":{"offset":0,"limit":30,"total":0}},"data":[]}""", status_code=200)
    elif args[0].startswith('https://studip.uni-hannover.de/jsonapi.php/v1/study-areas/'):
        with open('tests/resources/LUHStudIp_001_root_study_area.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class LUHStudIpImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        LUHStudIpImport(config=get_test_config()).process()
        self.assertGreaterEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/LUHStudIp_expected_001.json')


if __name__ == '__main__':
    unittest.main()
