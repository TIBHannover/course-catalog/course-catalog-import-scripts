import unittest
from unittest import mock
from EnlightImport import EnlightImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://courses.enlight-eu.org/api/courses':
        if kwargs["params"]["pageNumber"] == 1:
            with open('tests/resources/Enlight_001.json') as f:
                return MockResponse(content=f.read(), status_code=200)
        else:
            return MockResponse(content="""{ "pageSize":100,"pageNumber":1000,"hasPreviousPage":true,"hasNextPage":false,"totalResults":0,"items":[]}""", status_code=200)
    elif args[0].startswith('https://courses.enlight-eu.org/api/organizations/'):
        with open('tests/resources/Enlight_001_orga.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class EnlightImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        EnlightImport().process()
        self.assertGreaterEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/Enlight_expected_001.json')
        self.assert_result_contains_expected_metadata(result_metadata[1], 'tests/resources/Enlight_expected_002.json')


if __name__ == '__main__':
    unittest.main()
