import unittest
from unittest import mock
from enhanceImport import EnhanceImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://educationpathways.enhanceuniversity.eu/enhance/rest/course/filters':
        with open('tests/resources/enhance_001_filters.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0].startswith('https://educationpathways.enhanceuniversity.eu/enhance/rest/course/details'):
        with open('tests/resources/enhance_001_course_details.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    if args[0] == 'https://educationpathways.enhanceuniversity.eu/enhance/rest/course/':
        with open('tests/resources/enhance_001_courses.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    return default_backend_mock_requests_post(args, kwargs)


class EnhanceImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        EnhanceImport().process()
        self.assertEqual(mock_post.call_count, 2)
        result_metadata = mock_post.call_args_list[1][1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/enhance_expected_001.json')


if __name__ == '__main__':
    unittest.main()
