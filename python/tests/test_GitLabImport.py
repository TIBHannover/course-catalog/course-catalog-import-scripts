import base64
import json
import unittest
from unittest import mock
from GitLabImport import GitLabComImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse, \
    default_backend_mock_requests_delete


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://gitlab.com/api/v4/projects':
        with open('tests/resources/gitlab_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://gitlab.com/api/v4/projects/test%2Fgit-beispiel':
        with open('tests/resources/gitlab_single_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://gitlab.com/api/v4/projects/12345678/repository/files/metadata.yml':
        with open('tests/resources/gitlab_001_metadata.yml') as f:
            content = json.dumps({"content": base64.b64encode(f.read().encode("utf-8")).decode("utf-8")})
            return MockResponse(content=content, status_code=200)
    elif args[0] == 'https://gitlab.com/api/v4/projects/12345680/repository/files/metadata.yml':
        with open('tests/resources/gitlab_002_metadata.yml') as f:
            content = json.dumps({"content": base64.b64encode(f.read().encode("utf-8")).decode("utf-8")})
            return MockResponse(content=content, status_code=200)
    elif args[0] == 'https://gitlab.com/api/v4/projects/test%2FnotFound':
        return MockResponse(status_code=404)
    elif args[0] == 'https://gitlab.com/api/v4/projects/test%2FnoMetadata':
        with open('tests/resources/gitlab_single_002.json') as f:
            return MockResponse(content=f.read(), status_code=200)
    elif args[0] == 'https://gitlab.com/api/v4/projects/12345679/repository/files/metadata.yml':
        return MockResponse(status_code=404, content=json.dumps({"message": "404 Project Not Found"}))

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class GitLabImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        GitLabComImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assertEqual(len(result_metadata), 2)
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/gitlab_expected_001.json')
        self.assert_result_contains_expected_metadata(result_metadata[1], 'tests/resources/gitlab_expected_002.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_single_import(self, mock_post, mock_get):
        GitLabComImport().process_single_update("https://gitlab.com/test/git-beispiel")
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/gitlab_expected_001.json')

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    def test_deletion_via_single_import(self, mock_delete, mock_get):
        GitLabComImport().process_single_update("https://gitlab.com/test/notFound")
        self.assertEqual(mock_delete.call_count, 1)

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.delete', side_effect=default_backend_mock_requests_delete)
    def test_deletion_via_single_import_for_missing_metadata(self, mock_delete, mock_get):
        GitLabComImport().process_single_update("https://gitlab.com/test/noMetadata")
        self.assertEqual(mock_delete.call_count, 1)


if __name__ == '__main__':
    unittest.main()
