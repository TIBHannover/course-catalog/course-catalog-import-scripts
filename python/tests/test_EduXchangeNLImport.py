import unittest
from unittest import mock
from EduXchangeNLImport import EduXchangeNLImport
from search_index_import_commons.test.BaseImportTest import BaseImportTest, default_backend_mock_requests_post, MockResponse


def mocked_requests_get(*args, **kwargs):
    if args[0] == 'https://eduxchange.nl':
        return MockResponse(content="""<html><body><script id="__NEXT_DATA__" type="application/json">{"buildId":"abcdefghijkl"}</script></body></html>""", status_code=200)
    elif args[0] == 'https://eduxchange.nl/_next/data/abcdefghijkl/en/guest/explore.json':
        with open('tests/resources/EduXchangeNL_001.json') as f:
            return MockResponse(content=f.read(), status_code=200)

    return MockResponse(status_code=404)


def mocked_requests_post(*args, **kwargs):
    return default_backend_mock_requests_post(args, kwargs)


class EduXchangeNLImportTest(BaseImportTest):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    @mock.patch('requests.post', side_effect=mocked_requests_post)
    def test_import(self, mock_post, mock_get):
        EduXchangeNLImport().process()
        self.assertEqual(mock_post.call_count, 1)
        result_metadata = mock_post.call_args[1]["json"]
        self.assert_result_contains_expected_metadata(result_metadata[0], 'tests/resources/EduXchangeNL_expected_001.json')


if __name__ == '__main__':
    unittest.main()
