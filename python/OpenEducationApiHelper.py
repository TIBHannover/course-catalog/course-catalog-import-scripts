import abc
import logging
import re
import requests
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter

from Helpers import OrganizationLogoHelper
from OpenEducationApiConverter import OpenEducationApiConverter


# Helper for import of sources that provide the OOAPI (Open Education API)
# see also https://openonderwijsapi.nl/specification/v5/docs.html


class OpenEducationApiImportBase(SearchIndexImporter):
    def __init__(self, name, server_root_url, api_base_path='', mapping=None, items_per_request=100, preferred_language='en'):
        super().__init__(mapping=mapping)
        self.name = name
        self.server_root_url = server_root_url
        self.api_url = server_root_url + api_base_path
        self.organizations_by_id = {}
        self.items_per_request = items_per_request
        self.preferred_language = preferred_language
        self.page = 1
        self.default_headers = {"Accept": "application/json", "Content-Type": "application/json", "User-Agent": self.user_agent}
        self.converter = OpenEducationApiConverter(mapping=mapping)
        self.organization_logo_helper = OrganizationLogoHelper(self.config)

    def get_name(self):
        return self.name

    def contains_record(self, record_id):
        return record_id.startswith(self.server_root_url)

    def load_single_record(self, record_id):
        url_match = re.match("^" + self.api_url + "/courses/([a-z0-9-]+)", record_id)
        if url_match:
            resp = requests.get(record_id, headers=self.default_headers)
            if resp.status_code != 200:
                return {"response_status_code": 404}
            record = resp.json()
            return self.enrich_input(record)[0]
        return None

    def retrieve_organization_metadata(self, organization_id):
        if not organization_id:
            return None
        if organization_id in self.organizations_by_id:
            return self.organizations_by_id[organization_id]
        resp = requests.get(self.api_url + "/organizations/" + organization_id, headers=self.default_headers)
        if resp.status_code != 200:
            logging.info("Cannot retrieve organization info for %s", organization_id)
            return None
        self.organizations_by_id[organization_id] = resp.json()
        return self.organizations_by_id[organization_id]

    def enrich_input(self, record):
        org_meta = self.retrieve_organization_metadata(record["organization"])
        if org_meta:
            record["organization"] = org_meta
        # TODO offerings could be added here via GET /courses/{courseId}/offerings
        course_offerings = [{"course": record}]
        return course_offerings

    def load_next(self):
        query = {"pageSize": self.items_per_request, "pageNumber": self.page}
        # this is not a standard OOAPI endpoint, but it is available (use GET /api/courses for example)
        resp = requests.get(self.api_url + "/courses", headers=self.default_headers, params=query)
        if resp.status_code != 200:
            raise IOError("Could not fetch records from " + self.get_name() + ": {}".format(resp))
        self.page += 1
        records = resp.json()["items"]
        if len(records) == 0:
            return None
        course_offerings = []
        for record in records:
            course_offerings.extend(self.enrich_input(record))
        return course_offerings

    def transform(self, data: list) -> list:
        return list(filter(None, map(lambda record: self.enrich_output(self.to_search_index_metadata(record)), data)))

    def enrich_output(self, record):
        self.organization_logo_helper.set_default_logo(record)
        return record

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""

    def map_default(self, record):
        meta = self.converter.default_transformation(record)
        meta["id"] = self.api_url + "/courses/" + record["course"]["courseId"]
        meta["mainEntityOfPage"] = [
            {
                "id": self.api_url + "/courses/" + record["course"]["courseId"],
                "provider": {
                    "id": self.server_root_url,
                    "type": "Service",
                    "name": self.get_name()
                }
            }
        ]
        return meta
