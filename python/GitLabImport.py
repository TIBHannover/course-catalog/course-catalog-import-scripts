from GitLabHelpers import GitLabImportBase


class GitLabComImport(GitLabImportBase):
    def __init__(self):
        super().__init__("GitLab", "gitlab.com")


if __name__ == "__main__":
    GitLabComImport().process()
