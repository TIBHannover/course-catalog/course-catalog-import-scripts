from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping


class EnlightMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("org_id", {
            "Comenius University Bratislava": "https://ror.org/0587ef340",
            "Georg-August-Universität Göttingen": "https://ror.org/01y9bpm73",
            "Ghent University": "https://ror.org/00cv9y106",
            "Tartu Ülikool": "https://ror.org/03z77qz90",
            "Universidad del País Vasco / Euskal Herriko Unibertsitatea": "https://ror.org/000xsnr85",
            "Université de Bordeaux": "https://ror.org/057qpr032",
            "University of Groningen": "https://ror.org/012p63287"
        }, None)
