from search_index_import_commons.GitMetadataConverter import GitMetadataConverter
from search_index_import_commons.Helpers import get
import SchemaOrgConverter
from Helpers import OrganizationLogoHelper
from OpenEducationApiConverter import OpenEducationApiConverter


def contains_schema_org_course(metadata) -> bool:
    return "@context" in metadata and metadata["@context"] == "https://schema.org/"


class Converter(GitMetadataConverter):

    def __init__(self, config=None):
        self.ooapi_converter = OpenEducationApiConverter(fields_of_study_prefix="https://w3id.org/kim/hochschulfaechersystematik/")
        self.organization_logo_helper = OrganizationLogoHelper(config)

    def transform(self, record, provider_name, provider_id):
        return self.enrich(self.__convert_record__(record, provider_name, provider_id))

    def enrich(self, record):
        self.organization_logo_helper.set_default_logo(record)
        return record

    def __convert_record__(self, record, provider_name, provider_id):
        if contains_schema_org_course(record):
            meta = SchemaOrgConverter.transform_metadata(record)
        else:
            meta = self.ooapi_converter.default_transformation(record)
        meta["mainEntityOfPage"] = [
            {
                "id": get(record["git_metadata"], "source_url"),
                "provider": {
                    "type": "Service",
                    "name": provider_name,
                    "id": provider_id
                }
            }
        ]
        if get(record["git_metadata"], "hasVersion"):
            meta["hasVersion"] = get(record["git_metadata"], "hasVersion")
        return meta
