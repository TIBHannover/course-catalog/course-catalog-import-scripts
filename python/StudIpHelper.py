import abc
import base64
import html2text
import logging
import requests
from search_index_import_commons.Helpers import get
from search_index_import_commons.SearchIndexImporter import SearchIndexImporter

from Helpers import OrganizationLogoHelper


class StudIpImportBase(SearchIndexImporter):
    def __init__(self, name, base_url, study_areas, mapping=None, config=None):
        super().__init__(mapping=mapping, config=config)
        self.name = name
        self.base_url = base_url
        self.root_study_areas = study_areas
        self.full_study_area_infos = None
        self.categories_by_course_id = {}
        user = self.config[self.get_name()]["user"]
        password = self.config[self.get_name()]["password"]
        credentials = base64.b64encode((user + ":" + password).encode("utf-8")).decode("utf-8")
        # self.headers = {"User-Agent": self.user_agent, "authorization": "Basic " + credentials}   # custom user agent is rejected by Stud.IP
        self.headers = {"authorization": "Basic " + credentials}
        self.html2text = html2text.HTML2Text(bodywidth=0)
        self.html2text.ignore_links = True
        self.html2text.ignore_emphasis = True
        self.organization_logo_helper = OrganizationLogoHelper(self.config)

    def get_name(self):
        return self.name

    def contains_record(self, record_id):
        # TODO implement
        return False

    def load_single_record(self, record_id):
        # TODO implement
        return None

    def load_next(self):
        return self.load_by_study_areas()

    def get_tree_study_area_info_for_root_areas(self, root_study_areas):
        result = []
        for root_area in root_study_areas:
            if "name" not in root_area or "coursesLink" not in root_area:
                study_area_resp = requests.get(self.base_url + "/jsonapi.php/v1/study-areas/" + root_area["id"], headers=self.headers)
                study_area_resp_json = study_area_resp.json()
                root_area["name"] = study_area_resp_json["data"]["attributes"]["name"]
                root_area["coursesLink"] = study_area_resp_json["data"]["relationships"]["courses"]["links"]["related"]
            categories = root_area["parent_categories"] if "parent_categories" in root_area else []
            categories.append(root_area["name"])
            result.append({"id": root_area["id"], "name": root_area["name"], "coursesLink": root_area["coursesLink"], "categories": categories})

            empty_response = False
            offset = 0
            children = []
            items_per_request = 100
            while not empty_response:
                params = {"page[limit]": items_per_request, "page[offset]": offset}
                children_resp = requests.get(self.base_url + "/jsonapi.php/v1/study-areas/" + root_area["id"] + "/children", headers=self.headers, params=params).json()
                offset += items_per_request
                if children_resp["data"]:
                    children.extend(list(map(lambda x: {
                        "id": x["attributes"]["id"],
                        "name": x["attributes"]["name"],
                        "coursesLink": x["relationships"]["courses"]["links"]["related"],
                        "parent_categories": categories.copy()
                    }, children_resp["data"])))
                else:
                    empty_response = True
            if children:
                result.extend(self.get_tree_study_area_info_for_root_areas(children))
        return result

    def load_by_study_areas(self):
        if self.full_study_area_infos is None:
            self.full_study_area_infos = self.get_tree_study_area_info_for_root_areas(self.root_study_areas)
            logging.debug("Loaded %s study area infos", len(self.full_study_area_infos))

        while self.full_study_area_infos:
            study_area = self.full_study_area_infos.pop()
            items_per_request = 50
            params = {"page[limit]": items_per_request, "page[offset]": study_area["offset"] if "offset" in study_area else 0}
            courses_resp = requests.get(self.base_url + study_area["coursesLink"], headers=self.headers, params=params)
            courses_resp_json = courses_resp.json()
            study_area["offset"] = courses_resp_json["meta"]["page"]["offset"] + items_per_request
            if courses_resp_json["data"]:
                self.full_study_area_infos.append(study_area)
                courses = list(filter(lambda r: self.filter_resource(r), courses_resp_json["data"]))
                for course in courses:
                    categories = study_area["categories"]
                    if course["id"] in self.categories_by_course_id:
                        logging.debug("Course was already processed: " + course["id"])
                        self.stats.add_successes(-1)
                        categories.extend(self.categories_by_course_id[course["id"]])
                        categories = list(set(categories))
                        logging.debug("Extended categories to %s",  categories)
                    self.categories_by_course_id[course["id"]] = categories
                    course["categories"] = categories
                return courses
        return None

    def transform(self, data: list) -> list:
        return list(filter(None, map(lambda record: self.enrich(self.to_search_index_metadata(record)), data)))

    def enrich(self, record):
        self.organization_logo_helper.set_default_logo(record)
        return record

    @abc.abstractmethod
    def filter_resource(self, record):
        """instance specific filter for record"""

    @abc.abstractmethod
    def to_search_index_metadata(self, record):
        """instance specific transformation of a record"""

    def map_default(self, record):
        attributes = get(record, "attributes")
        subjects = list(set(filter(None, map(lambda x: self.mapping.get("subject", x), record["categories"])))) if "categories" in record else []
        levels = list(set(filter(None, map(lambda x: self.mapping.get("educational_level", x), record["categories"])))) if "categories" in record else []
        meta = {
            "@context": "https://schema.org",
            "type": ["Course"],
            "id": self.base_url + "/dispatch.php/course/details/?sem_id=" + record["id"],
            "name": [{"@value": get(attributes, "title")}],
            "description": [{"@value": self.html2text.handle(get(attributes, "description")).strip()}] if get(attributes, "description") is not None else None,
            "courseCode": record["id"],
            "about": list(map(lambda x: {"id": x}, subjects)),
            "educationalLevel": levels,
            "mainEntityOfPage": [
                {
                    "id": self.base_url + "/jsonapi.php/v1/courses/" + record["id"],
                    "provider": {
                        "id": self.base_url,
                        "type": "Service",
                        "name": self.get_name()
                    }
                }
            ]
        }
        return meta
