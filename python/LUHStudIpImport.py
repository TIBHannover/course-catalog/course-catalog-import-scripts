from search_index_import_commons.Helpers import get
from LUHStudIpMapping import LUHStudIpMapping
from StudIpHelper import StudIpImportBase


class LUHStudIpImport(StudIpImportBase):
    def __init__(self, config=None):
        super().__init__("LUH Stud.IP", "https://studip.uni-hannover.de",
                         [
                             {"id": "3daf944d613872f93092fbe433c167ab"},  # WiSe 2023/2024 Mathematik und Physik
                             {"id": "67d1243c79319ac2a9f0d12d41dba0f0"},  # WiSe 2023/2024 Maschinenbau
                             {"id": "bd926f3835b1b338c1ea9a210566bb6a"},  # WiSe 2023/2024 Elektrotechnik und Informatik
                             {"id": "b62e686393ab87b882dc0b1c9601f63b"}  # WiSe 2023/2024 Bauingenieurwesen und Geodäsie
                         ],
                         LUHStudIpMapping(),
                         config=config)

    def to_search_index_metadata(self, record):
        meta = self.map_default(record)
        meta["sourceOrganization"] = [{"type": "Organization", "name": "Gottfried Wilhelm Leibniz Universität Hannover", "id": "https://ror.org/0304hq317"}]
        return meta

    def filter_resource(self, record):
        if not get(get(record, "attributes"), "description"):
            return False
        return True


if __name__ == "__main__":
    LUHStudIpImport().process()
