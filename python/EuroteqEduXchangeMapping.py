from search_index_import_commons.mapping import iso639_2_to_iso639_1
from search_index_import_commons.SearchIndexImportMapping import SearchIndexImportMapping
import search_index_import_commons.constants.KimHochschulfaechersystematik as Subject


class EuroteqEduXchangeMapping(SearchIndexImportMapping):
    def __init__(self):
        self.add_mapping("subject", {
            "Architecture_and_Construction": Subject.N013_ARCHITECTURE,
            "Business_and_Economics": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Chemistry_and_Biology": Subject.N4_MATHEMATICS_NATURAL_SCIENCES,
            "Computer_Science_and_ICT_Data_AI": Subject.N71_COMPUTER_SCIENCE,
            "Electrical_Engineering": Subject.N64_ELECTRICAL_ENGINEERING_AND_INFORMATION_ENGINEERING,
            "Entrepreneurship": Subject.N30_BUSINESS_AND_ECONOMICS,
            "Food_and_Health_Sciences_Medical_engineering": Subject.N48_HEALTH_SCIENCES_GENERAL,
            "Languages_and_Culture": Subject.N1_HUMANITIES,
            "Manufacturing_and_Processing": Subject.N202_MANUFACTURING_TECHNOLOGY_PRODUCTION_ENGINEERING,
            "Mathematics_and_Statistics": Subject.N37_MATHEMATICS,
            "Mechanical_Engineering": Subject.N63_MECHANICAL_ENGINEERING__PROCESS_ENGINEERING,
            "Other_subject_area": None,
            "Physics_and_Energy": Subject.N128_PHYSICS,
            "Transport": Subject.N089_TRANSPORT_ENGINEERING,
        }, None)

        self.add_mapping("org_id", {
            "Czech Technical University in Prague": "https://ror.org/03kqpb082",
            "École Polytechnique": "https://ror.org/05hy3tk52",
            "École Polytechnique Fédérale de Lausanne": "https://ror.org/02s376052",
            "Eindhoven University of Technology": "https://ror.org/02c2kyt77",
            "Institut Polytechnique de Paris": "https://ror.org/042tfbd02",
            "Tallinn University of Technology": "https://ror.org/0443cwa12",
            "Technical University of Denmark": "https://ror.org/04qtj9h94",
            "Technical University of Munich": "https://ror.org/02kkvpp62",
            "Technion - Israel Institute of Technology": "https://ror.org/03qryx823",
        }, None)

        self.add_mapping("language", iso639_2_to_iso639_1.mapping, None)

        self.add_mapping("educational_level", {
            "bachelor": "https://w3id.org/kim/educationalLevel/level_6",
            "Bachelor": "https://w3id.org/kim/educationalLevel/level_6",
            "doctoral": "https://w3id.org/kim/educationalLevel/level_8",
            "Doctoral": "https://w3id.org/kim/educationalLevel/level_8",
            "master": "https://w3id.org/kim/educationalLevel/level_7",
            "Master": "https://w3id.org/kim/educationalLevel/level_7",
        })
