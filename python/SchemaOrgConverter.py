from search_index_import_commons.Helpers import get, get_list, filter_none_values
from Helpers import map_credits


def get_controlled_vocab_entry(s, transform):
    if isinstance(s, str):
        identifier = transform(s)
        return None if identifier is None else {"id": identifier}
    return s


def init_controlled_vocab_list(record, field_name, transform=lambda x: x):
    return list(filter(None, map(lambda s: get_controlled_vocab_entry(s, transform), get_list(record, field_name))))


def get_person_name(person):
    if "name" in person:
        return person["name"]
    names = []
    if "givenName" in person:
        names.append(person["givenName"])
    if "familyName" in person:
        names.append(person["familyName"])
    return " ".join(names) if len(names) > 0 else None


def get_organizations(organizations_record, max_level=2):
    if not organizations_record or max_level <= 0:
        return None
    return list(map(lambda organization_record: filter_none_values({
        "name": get(organization_record, "name"),
        "type": "Organization",
        "id": get(organization_record, "id"),
        "additionalType": get(organization_record, "additionalType"),
        "subOrganization": get_organizations(get_list(organization_record, "subOrganization"), max_level=max_level-1)
    }), organizations_record))


def transform_metadata(record):
    course_instance = None
    course_instance_record = get(record, "hasCourseInstance")
    if course_instance_record:
        instructors = get_list(course_instance_record, "instructor")
        course_instance = filter_none_values({
            "courseMode": get_list(course_instance_record, "courseMode"),
            "courseWorkload": get(course_instance_record, "courseWorkload"),
            "location": get(course_instance_record, "location"),
            "startDate": get(course_instance_record, "startDate"),
            "endDate": get(course_instance_record, "endDate"),
            "instructor": list(
                map(lambda x: filter_none_values({"name": get_person_name(x), "type": "Person", "id": get(x, "id")}), instructors)),
        })
    meta = {
        "@context": "https://schema.org",
        "type": ["Course"],
        "name": [{"@value": record["name"]}],
        "id": record["id"],
        "description": [{"@value": record["description"]}],
        "educationalLevel": get_list(record, "educationalLevel"),
        "courseCode": get(record, "courseCode"),
        "numberOfCredits": map_credits(get(record, "numberOfCredits")),
        "coursePrerequisites": get_list(record, "coursePrerequisites"),
        "about": init_controlled_vocab_list(record, "about"),
        "inLanguage": get_list(record, "inLanguage"),
        "teaches": get_list(record, "teaches"),
        "sourceOrganization": get_organizations(get_list(record, "sourceOrganization")),
        "hasPart": get_list(record, "hasPart"),
        "hasCourseInstance": course_instance
    }
    return meta
