from search_index_import_commons.GitLabImporterBase import GitLabImporterBase
from GitMetadataConverter import Converter, contains_schema_org_course


class GitLabImportBase(GitLabImporterBase):
    def __init__(self, name, gitlab_domain, default_gitlab_pages_domain=None):
        super().__init__(name, gitlab_domain, None, "University Courses", default_gitlab_pages_domain)
        self.converter = Converter(self.config)
        self.allowed_statuses = self.config["git"]["allowed_statuses"] if "git" in self.config and "allowed_statuses" in self.config["git"] else ["Published"]

    def should_import(self, metadata) -> bool:
        if contains_schema_org_course(metadata):
            return "creativeWorkStatus" in metadata and metadata["creativeWorkStatus"] in self.allowed_statuses
        elif "ext" in metadata and "status" in metadata["ext"]:
            return metadata["ext"]["status"] in self.allowed_statuses
        return True
