# Course Catalog Import Scripts

Contains the scripts to import the distributed resources into the search index.

Uses https://gitlab.com/oersi/sidre/sidre-import-scripts-commons for easier connection to the backend and common helper classes.

## Python

### Requirements

* python3
* see [requirements.txt](python/requirements.txt)

### Setup

1. Clone using ssh
2. In root directory, create virtual env: `python3 -m venv .venv`
3. `cd .venv/bin`
4. `./pip3 install -r ../../python/requirements.txt`

### Configuration

* `config/config.yml`
    * configure access of search index backend
* `config/logging.conf`
    * logging configuration (see https://docs.python.org/3/library/logging.html)

### Commands

Execute command from directory `python`

* Run import `python3 <Importscript>.py`
* Run tests `python3 -m unittest tests/test_**.py`
* Run tests with test-coverage `coverage3 run -m unittest tests/test_**.py`

### Common Features

See https://gitlab.com/oersi/sidre/sidre-import-scripts-commons

### Additional Configuration

| key                                     | type          | description                                                                     |
|-----------------------------------------|---------------|---------------------------------------------------------------------------------|
| CourseCatalogue                         | object        | Specific configuration for the Course Catalogue - attributes defined below      |
| CourseCatalogue.OrganizationLogoMapping | object / dict | Map ror-Id of organizations to logo-url. These logos are used as default image. |
