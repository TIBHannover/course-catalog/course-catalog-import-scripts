### Service URL and Access Point
(How can the source be accessed? Please add API/OAI-PMH/Sitemap documentation and/or an example)

### Description
(Which source of courses should be connected?)

### Available metadata

#### Essential data for filtering

- [ ] `subject`
- [ ] `course mode`
- [ ] `level`
- [ ] `sourceOrganization`
- [ ] `instructor`
- [ ] `credits`
- [ ] `language`

#### display metadata

- [ ] `id`
- [ ] `name`
- [ ] `description`
- [ ] `startDate` / `endDate`
- [ ] `coursePrerequisites`
- [ ] `learningOutcomes`

/label add-source